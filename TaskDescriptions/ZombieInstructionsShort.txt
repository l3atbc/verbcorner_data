Philosophers have long debated the existence of "philosophical zombies." A philosophical zombie looks like a person and acts like a person, but there's nothing going on inside. Unlike you or me, a philosophical zombie is never happy or sad and has no beliefs or desires. The zombie may *act* as if s/he is happy or sad or believes this and wants that, but it's all just an illusion. 

It turns out that philosophical zombies really exist! The government has decided to take an accurate census and figure out who is a real human and who is a zombie, and YOU have been put in charge. But how do you do this if zombies look and act just like real humans? 

Luckily, there is an Oracle. Everything the Oracle says is true. Sometimes, you can use what the Oracle says to prove that somebody is a real human and not a zombie. For instance, if the Oracle says that Sally is angry or wants to eat ice cream, Sally must not be a zombie, because zombies can't actually be angry or want anything. However, if the Oracle says that Sally yelled at someone or ate ice cream, you can't make a decision one way or another, since zombies can yell and eat ice cream, too. 

The government is worried about impartiality. It wouldn't do for you to claim anyone you don't like is a zombie! So the reports you get from the Oracle have been anonymized. People's names have been changed randomly (so "Mary" in one report may not be the "Mary" in another), and many of the other words have similarly been randomly replaced with made-up words (what "blicket" means in one report may not be what it means in another). 

Summary:
1) Read the Oracle's pronouncements.
2) Decide whether anyone mentioned is definitely a real human (NOT a zombie).
3) You may be able to prove that multiple people are definitely real humans (NOT zombies).
4) Everyone's name has been changed, and names of objects and other words have been replaced with made-up words.